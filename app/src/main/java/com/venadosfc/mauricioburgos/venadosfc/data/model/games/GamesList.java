package com.venadosfc.mauricioburgos.venadosfc.data.model.games;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Mauricio Burgos on 09/11/2018.
 */

public class GamesList {
    //The web service response a list of games.
    @SerializedName("games")
    private ArrayList<Games> gamesList;

    public GamesList() {
    }

    public GamesList(ArrayList<Games> gamesList) {
        this.gamesList = gamesList;
    }

    public ArrayList<Games> getGames() {
        return gamesList;
    }

    public void setGames(ArrayList<Games> gamesList) {
        this.gamesList = gamesList;
    }
}
