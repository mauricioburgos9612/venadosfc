package com.venadosfc.mauricioburgos.venadosfc.views.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.Constants;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.SharedPreferencesHelper;
import com.venadosfc.mauricioburgos.venadosfc.data.model.players.Players;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Mauricio Burgos on 11/11/2018.
 */

public class FragmentPlayerDetails extends DialogFragment {

    private TextView name;
    private TextView position;
    private TextView birthdate;
    private TextView birthplace;
    private TextView weight;
    private TextView height;
    private TextView lastTeam;
    private ImageView image;
    private SharedPreferencesHelper sharedPreferencesHelper;
    private View view;
    Transformation transformation;
    Context context;

	public FragmentPlayerDetails() {
            // Empty constructor is required for DialogFragment
            // Make sure not to add arguments to the constructor
            // Use `newInstance` instead as shown below
        }

    public static FragmentPlayerDetails newInstance(String title) {
        FragmentPlayerDetails  frag= new FragmentPlayerDetails();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_player_details, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.sharedPreferencesHelper        = new SharedPreferencesHelper();

        name = (TextView) view.findViewById(R.id.playerName);
        position = (TextView) view.findViewById(R.id.playerPosition);
        birthdate = (TextView) view.findViewById(R.id.birthdate);
        birthplace =(TextView) view.findViewById(R.id.birthplace);
        weight =(TextView) view.findViewById(R.id.weight);
        height =(TextView) view.findViewById(R.id.height);
        lastTeam =(TextView) view.findViewById(R.id.lastTeam);
        image =(ImageView) view.findViewById(R.id.playerImage);

        Gson gson = new Gson();
        String json = sharedPreferencesHelper.get(Constants.STRING_PLAYER,"");
        final Players players = gson.fromJson(json, Players.class);

        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");

        // Set the date to the holder
        Date theDate = null;
        try {
            theDate = simpleDateFormat.parse(players.getBirthday());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final Date finalTheDate = theDate;

        Calendar myCal = new GregorianCalendar();
        myCal.setTime(theDate);

        name.setText(players.getName());
        if(players.getPosition()==null)
        {
            position.setText(players.getRole());

        }
        else
        {
            position.setText(players.getPosition());

        }
        birthdate.setText(String.valueOf(myCal.get(Calendar.DATE))+"/"+String.valueOf(myCal.get(Calendar.MONTH)+1)+"/"+String.valueOf(myCal.get(Calendar.YEAR)));
        birthplace.setText(players.getBirthPlace());
        if(players.getWeight()==0)
        {
            weight.setText("No disponble");

        }
        else
        {
            weight.setText(String.valueOf(players.getWeight())+" KG");

        }
        if(players.getHeight()==0)
        {
            height.setText("No disponble");

        }
        else
        {
            height.setText(String.valueOf(players.getHeight())+" M");

        }
        if(players.getLastTeam()==null)
        {
            lastTeam.setText("No disponible");

        }
        else {
            lastTeam.setText(players.getLastTeam());

        }
        transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .cornerRadiusDp(90)
                .oval(false)
                .build();

        Picasso.get().load(players.getUrlImage()).placeholder(R.color.colorGreen).resize(400, 400).transform(transformation).into(image);


    }
}
