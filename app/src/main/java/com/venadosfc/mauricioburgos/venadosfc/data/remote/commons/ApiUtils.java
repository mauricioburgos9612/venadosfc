package com.venadosfc.mauricioburgos.venadosfc.data.remote.commons;


import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.Constants;
import com.venadosfc.mauricioburgos.venadosfc.data.remote.service.common.APIService;

import retrofit2.Retrofit;

public class ApiUtils {

    public ApiUtils(){}

    public static APIService getApiService(){
        return RetrofitClient.getClient(Constants.BASE_URL).create(APIService.class);
    }
    public static Retrofit getRetrofit(){
        return RetrofitClient.getClient(Constants.BASE_URL);
    }


}
