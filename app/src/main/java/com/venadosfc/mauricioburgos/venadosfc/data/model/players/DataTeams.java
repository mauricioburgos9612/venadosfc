package com.venadosfc.mauricioburgos.venadosfc.data.model.players;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mauricio Burgos on 10/11/2018.
 */

public class DataTeams {
    @SerializedName("team")
    private Positions positions;

    public DataTeams() {

    }

    public DataTeams(Positions positions) {
        this.positions = positions;
    }

    public Positions getPositions() {
        return positions;
    }

    public void setPositions(Positions positions) {
        this.positions = positions;
    }
}
