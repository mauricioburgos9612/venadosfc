package com.venadosfc.mauricioburgos.venadosfc.views.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.Constants;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.SharedPreferencesHelper;
import com.venadosfc.mauricioburgos.venadosfc.data.model.games.GamesList;
import com.venadosfc.mauricioburgos.venadosfc.data.model.games.GamesResponse;
import com.venadosfc.mauricioburgos.venadosfc.data.model.players.DataTeams;
import com.venadosfc.mauricioburgos.venadosfc.data.model.players.PlayersResponse;
import com.venadosfc.mauricioburgos.venadosfc.data.model.stats.ListStats;
import com.venadosfc.mauricioburgos.venadosfc.data.model.stats.StatsResponse;
import com.venadosfc.mauricioburgos.venadosfc.data.remote.commons.ApiUtils;
import com.venadosfc.mauricioburgos.venadosfc.data.remote.service.common.APIService;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends Activity {
    private static final int TIME = 2000;
    Animation frombotton;
    @BindView(R.id.logo) ImageView logo;
    private APIService mAPIService;
    GamesList gamesList = new GamesList();
    private SharedPreferencesHelper sharedPreferencesHelper;
    ListStats listStats = new ListStats();
    DataTeams dataTeams = new DataTeams();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.sharedPreferencesHelper        = new SharedPreferencesHelper();

        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        frombotton = AnimationUtils.loadAnimation(this,R.anim.frombotton);
        //Check internet Connection
        if(isOffline()){

            Toast.makeText(this,"No hay conexión a internet",Toast.LENGTH_SHORT).show();
            finish();

        }
        else {

            //Call the web service to get the games
            mAPIService = ApiUtils.getApiService();

            mAPIService = ApiUtils.getApiService();
            Call<StatsResponse> statsResponse = mAPIService.getStatistics();
            statsResponse.enqueue(new Callback<StatsResponse>() {
                @Override
                public void onResponse(Call<StatsResponse> call, Response<StatsResponse> response) {
                    if (response.body() instanceof StatsResponse) {

                        listStats = response.body().getData();
                        Gson gson = new Gson();
                        String json = gson.toJson(listStats); // myObject - instance of MyObject
                        sharedPreferencesHelper.put(Constants.STRING_STATS_LIST, json);


                    }
                }

                @Override
                public void onFailure(Call<StatsResponse> call, Throwable t) {

                }
            });
            Call<PlayersResponse> playersResponse = mAPIService.getPlayers();
            playersResponse.enqueue(new Callback<PlayersResponse>() {
                @Override
                public void onResponse(Call<PlayersResponse> call, Response<PlayersResponse> response) {
                    if (response.body() instanceof PlayersResponse) {

                        dataTeams = response.body().getData_teams();
                        Gson gson = new Gson();
                        String json = gson.toJson(dataTeams); // myObject - instance of MyObject
                        sharedPreferencesHelper.put(Constants.STRING_DATA_TEAMS, json);


                    }
                }

                @Override
                public void onFailure(Call<PlayersResponse> call, Throwable t) {

                }
            });
            final Call<GamesResponse> gamesResponse = mAPIService.getGames();
            gamesResponse.enqueue(new Callback<GamesResponse>() {
                @Override
                public void onResponse(Call<GamesResponse> call, final Response<GamesResponse> response) {

                    if (response.body() instanceof GamesResponse) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                GamesResponse gamesResponse = (GamesResponse) response.body();
                                gamesList = response.body().getData();

                                Gson gson = new Gson();
                                String json = gson.toJson(gamesList); // myObject - instance of MyObject
                                sharedPreferencesHelper.put(Constants.STRING_GAME_LIST, json);
                                Intent intent = new Intent(SplashScreen.this, NavigationActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                                finish();
                            }
                        }, TIME);


                    }

                }

                @Override
                public void onFailure(Call<GamesResponse> call, Throwable t) {

                }
            });


            logo.startAnimation(frombotton);
        }

    }


    private boolean isOffline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork == null || !activeNetwork.isConnected();
        }
        return true;
    }



}
