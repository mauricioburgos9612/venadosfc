package com.venadosfc.mauricioburgos.venadosfc.views.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.adapters.PlayerAdapter;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.Constants;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.SharedPreferencesHelper;
import com.venadosfc.mauricioburgos.venadosfc.data.model.players.DataTeams;
import com.venadosfc.mauricioburgos.venadosfc.data.model.players.Players;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentPlayers.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentPlayers#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPlayers extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    protected Context context;
    private DrawerLayout drawer;
    @BindView(R.id.playersToolbar)
    Toolbar toolbar;
    ArrayList<Players> centers;
    ArrayList<Players> forwards ;
    ArrayList<Players> defenses;
    ArrayList<Players> goalkeepers;
    ArrayList<Players> coaches;
    ArrayList<Players> all;
    private GridView gridView;
    private PlayerAdapter playerAdapter;
    private Spinner spinner;
    private static final String[] paths = {"Delanteros","Centros", "Defensas","Porteros","Coachs"};


    private SharedPreferencesHelper sharedPreferencesHelper;


    private OnFragmentInteractionListener mListener;

    public FragmentPlayers() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPlayers.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPlayers newInstance(String param1, String param2) {
        FragmentPlayers fragment = new FragmentPlayers();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.sharedPreferencesHelper        = new SharedPreferencesHelper();

        View view =inflater.inflate(R.layout.fragment_players, container, false);
        ButterKnife.bind(this, view);

        Gson gson = new Gson();
        String json = sharedPreferencesHelper.get(Constants.STRING_DATA_TEAMS,"");
        final DataTeams dataTeamsList = gson.fromJson(json, DataTeams.class);


        centers=dataTeamsList.getPositions().getCenters();
        forwards=dataTeamsList.getPositions().getForwards() ;
        defenses=dataTeamsList.getPositions().getDefenses();
        goalkeepers=dataTeamsList.getPositions().getGoalKeepers();
        coaches=dataTeamsList.getPositions().getCoaches();
        playerAdapter = new PlayerAdapter(defenses,getActivity().getApplicationContext(),getActivity().getSupportFragmentManager());

        gridView = view.findViewById(R.id.Players_grid);
        gridView.setAdapter(playerAdapter);

        //Spiner code
        spinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item,paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                ((TextView) parent.getChildAt(0)).setTextSize(15);
                if(position==0)
                {
                    playerAdapter = new PlayerAdapter(forwards,getActivity().getApplicationContext(),getActivity().getSupportFragmentManager());
                    gridView.setAdapter(playerAdapter);

                }
                if(position==1)
                {
                    playerAdapter = new PlayerAdapter(centers,getActivity().getApplicationContext(),getActivity().getSupportFragmentManager());
                    gridView.setAdapter(playerAdapter);
                }
                if(position==2)
                {
                    playerAdapter = new PlayerAdapter(defenses,getActivity().getApplicationContext(),getActivity().getSupportFragmentManager());
                    gridView.setAdapter(playerAdapter);
                }
                if(position==3)
                {
                    playerAdapter = new PlayerAdapter(goalkeepers,getActivity().getApplicationContext(),getActivity().getSupportFragmentManager());
                    gridView.setAdapter(playerAdapter);
                }
                if(position==4)
                {
                    playerAdapter = new PlayerAdapter(coaches,getActivity().getApplicationContext(),getActivity().getSupportFragmentManager());
                    gridView.setAdapter(playerAdapter);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        //Add the toolbar to the fragment

        final Drawable logo = getResources().getDrawable(R.drawable.ic_players);
        logo.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        toolbar.setTitle("Venados F.C");
        toolbar.setSubtitle("Jugadores");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        toolbar.setLogo(logo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        //give navigation drawer actions
        drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorWhite));
        toggle.syncState();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();


        super.onCreateOptionsMenu(menu,inflater);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
