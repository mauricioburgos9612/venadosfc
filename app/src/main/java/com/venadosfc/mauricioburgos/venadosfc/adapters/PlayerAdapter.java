package com.venadosfc.mauricioburgos.venadosfc.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.Constants;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.SharedPreferencesHelper;
import com.venadosfc.mauricioburgos.venadosfc.data.model.players.Players;
import com.venadosfc.mauricioburgos.venadosfc.views.fragments.FragmentPlayerDetails;

import java.util.ArrayList;

/**
 * Created by Mauricio Burgos on 11/11/2018.
 */

public class PlayerAdapter  extends BaseAdapter {
    private Context context;
    private FragmentManager fragmentManager;
    ArrayList<Players> players=new ArrayList<Players>();
    Transformation transformation;
    private SharedPreferencesHelper sharedPreferencesHelper;



    public PlayerAdapter(ArrayList<Players> players,Context context,FragmentManager fragmentManager) {
        this.players=players;
        this.context=context;
        this.fragmentManager=fragmentManager;


    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Players getItem(int i) {
        return  players.get(i);

    }


    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.player_layout, viewGroup, false);
        }
        this.sharedPreferencesHelper        = new SharedPreferencesHelper();

        ImageView imagePlayer = (ImageView) view.findViewById(R.id.Player_image);
        TextView positionPlayer = (TextView) view.findViewById(R.id.Player_Position);
        TextView namePlayer = (TextView) view.findViewById(R.id.Player_Name);
        final Players players = getItem(i);
        transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .cornerRadiusDp(60)
                .oval(false)
                .build();

        Picasso.get().load(players.getUrlImage()).placeholder(R.color.colorGreen).resize(300, 300).transform(transformation).into(imagePlayer);
        if(players.getPosition()==null){
            positionPlayer.setText(players.getRole());

        }else {
            positionPlayer.setText(players.getPosition());

        }
        namePlayer.setText(players.getName());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Gson gson = new Gson();
                String json = gson.toJson(players); // myObject - instance of MyObject
                sharedPreferencesHelper.put(Constants.STRING_PLAYER,json);
                FragmentPlayerDetails p = FragmentPlayerDetails.newInstance("Fragment");
                p.show(fragmentManager,"players_dialog_fragment");
            }
        });

        return view;
    }

}