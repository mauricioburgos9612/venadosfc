package com.venadosfc.mauricioburgos.venadosfc.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.data.model.stats.Stats;

import java.util.ArrayList;

/**
 * Created by Mauricio Burgos on 10/11/2018.
 */

public class StatsAdapter extends RecyclerView.Adapter<StatsAdapter.RecyclerViewHolder>{


    ArrayList<Stats> stats=new ArrayList<Stats>();

    public StatsAdapter(  ArrayList<Stats> stats)
    {
       this.stats=stats;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerViewHolder recyclerViewHolder;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.conten_recycler_stats, parent, false);
        recyclerViewHolder = new RecyclerViewHolder(view,viewType);
        return recyclerViewHolder;


    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Stats listStats;
        listStats= stats.get(position);

        holder.position.setText(String.valueOf(listStats.getPosition()));
        Picasso.get().load(listStats.getImage()).placeholder(R.color.colorGreen).into(holder.image);
        holder.team.setText(listStats.getTeam());
        holder.games.setText(String.valueOf(listStats.getGames()));
        holder.score_diff.setText(String.valueOf(listStats.getScore_diff()));
        holder.points.setText(String.valueOf(listStats.getPoints()));



    }



    @Override
    public int getItemCount() {
        return stats.size();
    }
    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        int view_type;
       //Variables for list
       private TextView position;
       private ImageView image;
       private TextView team;
       private TextView games;
       private TextView score_diff;
       private TextView points;



        public RecyclerViewHolder(View view,int viewType) {
            super(view);

                position =view.findViewById(R.id.position);
                image = view.findViewById(R.id.image);
                team = view.findViewById(R.id.team);
                games = view.findViewById(R.id.games);
                score_diff =view.findViewById(R.id.score_diff);
                points =view.findViewById(R.id.points);



        }
    }




}
