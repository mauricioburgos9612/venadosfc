package com.venadosfc.mauricioburgos.venadosfc.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.venadosfc.mauricioburgos.venadosfc.views.activities.MyApplication;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesHelper {

    private SharedPreferences sharedPreferences;
    private Context context;
    public SharedPreferencesHelper(){
        this.context = MyApplication.getByService();
        this.sharedPreferences = context.getSharedPreferences(Constants.NAME_SHAREDPREFERENCES, MODE_PRIVATE);

    }

    public void put(String key, String value){
        sharedPreferences.edit().putString(key,value).apply();
    }

    public void put(String key, Long value ){
        sharedPreferences.edit().putLong(key, value).apply();
    }
    public void put(String key, boolean bool)
    {
        sharedPreferences.edit().putBoolean(key,bool).apply();
    }
    public void put(String key, int value)
    {
        sharedPreferences.edit().putInt(key,value).apply();
    }

    public String get(String key, String value){
        return sharedPreferences.getString(key, value);
    }

    public int get(String key, int defaultValue){
        return sharedPreferences.getInt(key, defaultValue);
    }
    public long getLong(String key, long defaultValue){
        return sharedPreferences.getLong(key, defaultValue);
    }
    public boolean get(String key, boolean defaultValue)
    {
        return sharedPreferences.getBoolean(key,defaultValue);
    }

    public void deleteSavedData(String key){
        sharedPreferences.edit().remove(key).apply();
    }

}
