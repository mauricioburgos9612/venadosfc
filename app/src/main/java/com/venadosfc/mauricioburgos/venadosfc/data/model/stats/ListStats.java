package com.venadosfc.mauricioburgos.venadosfc.data.model.stats;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Mauricio Burgos on 10/11/2018.
 */

public class ListStats {
    @SerializedName("statistics")
    private ArrayList<Stats> stats = new ArrayList<>();

    public ListStats() {
    }

    public ListStats(ArrayList<Stats> stats) {

        this.stats = stats;
    }

    public ArrayList<Stats> getStatistics() {
        return stats;
    }

    public void setStatistics(ArrayList<Stats> statistics) {
        this.stats = stats;
    }
}
