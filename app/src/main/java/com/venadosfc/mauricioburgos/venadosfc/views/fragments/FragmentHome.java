package com.venadosfc.mauricioburgos.venadosfc.views.fragments;

import android.app.LauncherActivity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.adapters.GamesAdapter;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.Constants;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.SharedPreferencesHelper;
import com.venadosfc.mauricioburgos.venadosfc.data.model.games.Games;
import com.venadosfc.mauricioburgos.venadosfc.data.model.games.GamesList;
import com.venadosfc.mauricioburgos.venadosfc.data.model.games.GamesResponse;
import com.venadosfc.mauricioburgos.venadosfc.data.remote.commons.ApiUtils;
import com.venadosfc.mauricioburgos.venadosfc.data.remote.service.common.APIService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentHome.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentHome#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentHome extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    protected Context context;
    private DrawerLayout drawer;
    @BindView(R.id.homeToolbar)
    Toolbar toolbar;
    public View myRoot;
    @BindView(R.id.venadosLogo)
    ImageView venadosLogo;
    GamesList gamesList = new GamesList();
    TabLayout tabLayout;
    ArrayList<Games> games;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private GamesAdapter adapter;
    private APIService mAPIService;
    boolean copaMx=false;
    private SharedPreferencesHelper sharedPreferencesHelper;
    List<LauncherActivity.ListItem> consolidatedList = new ArrayList<>();
    // TODO: Rename and change types of parameters



    private OnFragmentInteractionListener mListener;

    public FragmentHome() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentHome.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentHome newInstance(String param1, String param2) {
        FragmentHome fragment = new FragmentHome();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }




    }

    @Override
    public void onResume() {

        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Create the class shared preferences helper for easy use of preferences
        this.sharedPreferencesHelper        = new SharedPreferencesHelper();

        // Inflate the layout for this fragment


        View view =inflater.inflate(R.layout.fragment_home, container, false);
        if (view== null)
        {
            Toast.makeText(getContext(),"Si",Toast.LENGTH_SHORT).show();

        }

        //bind the butterKnife
        ButterKnife.bind(this, view);


        tabLayout = view.findViewById(R.id.homeTabs);

        //Restoring the data game from the object we saved in shared preferences.
        Gson gson = new Gson();
        String json = sharedPreferencesHelper.get(Constants.STRING_GAME_LIST,"");
        final GamesList gamesList = gson.fromJson(json, GamesList.class);
        games=gamesList.getGames();




        //Add the toolbar to the fragment
        final Drawable logo = getResources().getDrawable(R.drawable.ic_home);
        logo.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        toolbar.setTitle("Venados F.C");
        toolbar.setSubtitle("Juegos");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        toolbar.setLogo(logo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);


        //give navigation drawer actions
        drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorWhite));

        toggle.syncState();

        //Add code to the tab layout and add the tabs to home
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.addTab(tabLayout.newTab().setText("COPA MX"));
        tabLayout.addTab(tabLayout.newTab().setText("ASCENSO MX"));
        FragmentCopaMx fragmentCopaMx=new FragmentCopaMx();
        replaceFragment(fragmentCopaMx,"fragmentCopaMx");
        copaMx=true;


        //add the tabs code for each case
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition())
                {
                    case 0:
                        copaMx=true;
                        FragmentCopaMx fragmentCopaMx=new FragmentCopaMx();
                        replaceFragment(fragmentCopaMx,"fragmentCopaMx");

                        break;
                    case 1:
                        copaMx=false;
                        FragmentAscensoMx fragmentAscensoMx= new FragmentAscensoMx();
                        replaceFragment(fragmentAscensoMx, "fragmentAscensoMx");
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //Swipe to refresh action
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                refreshData();

            }
        });




        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();


        super.onCreateOptionsMenu(menu,inflater);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void refreshData(){
        //Call the web service to get the games

        mAPIService = ApiUtils.getApiService();
        final Call<GamesResponse> gamesResponse = mAPIService.getGames();
        gamesResponse.enqueue(new Callback<GamesResponse>() {
            @Override
            public void onResponse(Call<GamesResponse> call, final Response<GamesResponse> response) {
                if(copaMx==true){
                    FragmentCopaMx fragmentCopaMx=new FragmentCopaMx();
                    replaceFragment(fragmentCopaMx,"fragmentCopaMx");
                }
                else
                {
                    FragmentAscensoMx fragmentAscensoMx= new FragmentAscensoMx();
                    replaceFragment(fragmentAscensoMx, "fragmentAscensoMx");
                }
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() instanceof GamesResponse) {

                            GamesResponse gamesResponse = (GamesResponse) response.body();
                            gamesList = response.body().getData();
                            Gson gson = new Gson();
                            String json = gson.toJson(gamesList); // myObject - instance of MyObject
                            sharedPreferencesHelper.put(Constants.STRING_GAME_LIST,json);




                }

            }

            @Override
            public void onFailure(Call<GamesResponse> call, Throwable t) {

            }
        });

    }

    public void replaceFragment (Fragment fragment, String tag){
        String backStateName = fragment.getClass().getName();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Fragment currentFragment= getActivity().getSupportFragmentManager().findFragmentById(R.id.fragmentHomeContainer);
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if(currentFragment != null){
                ft.hide(currentFragment);
            }
            ft.add(R.id.fragmentHomeContainer, fragment, tag);
            ft.addToBackStack(backStateName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
        }



}
