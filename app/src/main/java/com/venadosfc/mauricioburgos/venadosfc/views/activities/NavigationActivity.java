package com.venadosfc.mauricioburgos.venadosfc.views.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.data.remote.commons.ApiUtils;
import com.venadosfc.mauricioburgos.venadosfc.data.remote.service.common.APIService;
import com.venadosfc.mauricioburgos.venadosfc.views.fragments.FragmentHome;
import com.venadosfc.mauricioburgos.venadosfc.views.fragments.FragmentPlayers;
import com.venadosfc.mauricioburgos.venadosfc.views.fragments.FragmentStats;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private APIService mAPIService;
    Transformation transformation;
    View headerView;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);



        navigationView.setNavigationItemSelectedListener(this);
        headerView = navigationView.getHeaderView(0);
        ImageView navProfilePic = (ImageView) headerView.findViewById(R.id.venadosLogo);
        mAPIService = ApiUtils.getApiService();

        Picasso.get().load(R.drawable.logo).resize(400, 400).into(navProfilePic);






        setFragment(0);
        navigationView.getMenu().getItem(0).setChecked(true);



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            FragmentHome currentFragmentHome = (FragmentHome) getSupportFragmentManager().findFragmentByTag("homeFragment");
            if(currentFragmentHome!= null){
                finish();
            }
            else{
                navigationView.getMenu().getItem(0).setChecked(true);
                setFragment(0);
            }
        }

    }


    private boolean closeDrawer()
    {
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            setFragment(0);
            return closeDrawer();
        } else if (id == R.id.nav_statics) {
            setFragment(1);
            return closeDrawer();
        } else if (id == R.id.nav_players) {
            setFragment(2);
            return closeDrawer();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void setFragment(int position) {
        FragmentManager fragmentManager= getSupportFragmentManager();;
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();;
        switch (position) {
            case 0:

                FragmentHome currentFragmentHome = (FragmentHome) getSupportFragmentManager().findFragmentByTag("homeFragment");
                if (currentFragmentHome !=null && currentFragmentHome.isVisible()) { drawer.closeDrawer(GravityCompat.START); }
                  else {


                    FragmentHome fragmentHome = new FragmentHome();
                    fragmentTransaction.replace(R.id.fragmentContainer, fragmentHome,"homeFragment");
                    fragmentTransaction.commit();
                      }
                break;

            case 1:
                FragmentStats fS = (FragmentStats) getSupportFragmentManager().findFragmentByTag("staticsFragment");
                if (fS != null && fS.isVisible()) { drawer.closeDrawer(GravityCompat.START);}
                else {

                    FragmentStats fragmentStats = new FragmentStats();
                    fragmentTransaction.replace(R.id.fragmentContainer, fragmentStats,"staticsFragment");
                    fragmentTransaction.commit();
                }
                break;
            case 2:
                FragmentPlayers fP = (FragmentPlayers) getSupportFragmentManager().findFragmentByTag("playersFragment");
                if (fP != null && fP.isVisible()) { drawer.closeDrawer(GravityCompat.START);}
                else {

                    FragmentPlayers fragmentPlayers = new FragmentPlayers();
                    fragmentTransaction.replace(R.id.fragmentContainer, fragmentPlayers,"playersFragment");
                    fragmentTransaction.commit();;
                }

                break;

        }
    }






}
