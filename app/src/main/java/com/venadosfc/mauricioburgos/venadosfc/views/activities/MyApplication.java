package com.venadosfc.mauricioburgos.venadosfc.views.activities;

import android.app.Application;
import android.content.Context;


public class MyApplication extends Application {


    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
        //Use debug tools only in debug builds

    }


    public static MyApplication getByService() {
        return (MyApplication) context.getApplicationContext();
    }

}
