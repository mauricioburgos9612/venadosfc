package com.venadosfc.mauricioburgos.venadosfc.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.data.model.games.Games;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Mauricio Burgos on 10/11/2018.
 */

public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.MyViewHolder> {
    ArrayList<Games> games;
    Activity activity;
    private static final int TYPE_HEAD=0;
    private static final  int TYPE_LIST=1;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        int view_type;
        //Variables for list
        private ImageView local_image;
        private TextView local_name;
        private ImageView opponent_image;
        private TextView opponent_name;
        private TextView date;
        private TextView day;
        private ImageView ic_calendar;
        private TextView home_score;
        private TextView away_score;
        //Variables for head seaction
        private TextView stats_table;





        public MyViewHolder(View v,int viewType) {
            super(v);
            if(viewType==TYPE_LIST){
            local_image = itemView.findViewById(R.id.local_image);
            local_name = itemView.findViewById(R.id.home_name);
            opponent_image = itemView.findViewById(R.id.opponent_image);
            opponent_name = itemView.findViewById(R.id.opponent_name);
            ic_calendar = itemView.findViewById(R.id.calendar);
            date = itemView.findViewById(R.id.date);
            day = itemView.findViewById(R.id.day);
            home_score = itemView.findViewById(R.id.home_score);
            away_score = itemView.findViewById(R.id.away_score);
            view_type=1;
            }
            else if(viewType==TYPE_HEAD){
                stats_table= itemView.findViewById(R.id.stats_table);
                local_image = itemView.findViewById(R.id.local_image);
                local_name = itemView.findViewById(R.id.home_name);
                opponent_image = itemView.findViewById(R.id.opponent_image);
                opponent_name = itemView.findViewById(R.id.opponent_name);
                ic_calendar = itemView.findViewById(R.id.calendar);
                date = itemView.findViewById(R.id.date);
                day = itemView.findViewById(R.id.day);
                home_score = itemView.findViewById(R.id.home_score);
                away_score = itemView.findViewById(R.id.away_score);

                view_type=0;
            }

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)

    public GamesAdapter(ArrayList<Games> games,Activity activity) {
        this.games = games;
        this.activity = activity;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public GamesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v;
        if(viewType==TYPE_LIST) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_recycler_games,parent,false);
        GamesAdapter.MyViewHolder gamesViewHolder= new GamesAdapter.MyViewHolder(v,viewType);
        return gamesViewHolder;
        }
        else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.head_layout_statics, parent, false);
            GamesAdapter.MyViewHolder gamesViewHolder= new GamesAdapter.MyViewHolder(v,viewType);
            return gamesViewHolder;

        }


    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        if(holder.view_type==TYPE_LIST)
        {
            final Games game = games.get(position);

        //Check if game is local to setup the correct images for each view
        if(game.Local())
        {
            Picasso.get().load(game.getOpponent_image()).placeholder(R.color.colorGreen).into(holder.opponent_image);
            holder.opponent_name.setText(game.getOpponent());
            holder.local_image.setImageResource(R.drawable.logo);
            holder.local_name.setText("Venados F.C.");

        }else
        {
            Picasso.get().load(game.getOpponent_image()).placeholder(R.color.colorGreen).into(holder.local_image);
            holder.local_name.setText(game.getOpponent());
            holder.opponent_image.setImageResource(R.drawable.logo);
            holder.opponent_name.setText("Venados F.C.");

        }

        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");

        // Set the date to the holder
        Date theDate = null;
        try {
            theDate = simpleDateFormat.parse(game.getDatetime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final Date finalTheDate = theDate;
        holder.ic_calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                programToCalendar(finalTheDate);


            }
        });

        Calendar myCal = new GregorianCalendar();
        myCal.setTime(theDate);

        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
        Date date = null;
        try {
            date = inFormat.parse(game.getDatetime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
        String day = outFormat.format(date);
        String getDay = day.substring(0,3);



        holder.date.setText(String.valueOf(myCal.get(Calendar.DAY_OF_MONTH)));
        holder.day.setText(getDay.toUpperCase());


        holder.home_score.setText(String.valueOf(game.getHome_score()));
        holder.away_score.setText(String.valueOf(game.getAway_score()));
        }
        else if(holder.view_type==TYPE_HEAD){

            final Games game = games.get(position);
            Log.d("KHEE", String.valueOf(position));

            //Check if game is local to setup the correct images for each view
            if(game.Local())
            {
                SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
                Date date = null;
                try {
                    date = inFormat.parse(games.get(position).getDatetime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat outFormat = new SimpleDateFormat("MMMM");
                String month = outFormat.format(date);

                holder.stats_table.setText(month.toUpperCase());

                Picasso.get().load(game.getOpponent_image()).placeholder(R.color.colorGreen).into(holder.opponent_image);
                holder.opponent_name.setText(game.getOpponent());
                holder.local_image.setImageResource(R.drawable.logo);
                holder.local_name.setText("Venados F.C.");

            }else
            {

                SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
                Date date = null;
                try {
                    date = inFormat.parse(games.get(position).getDatetime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat outFormat = new SimpleDateFormat("MMMM");
                String month = outFormat.format(date);

                holder.stats_table.setText(month.toUpperCase());
                Picasso.get().load(game.getOpponent_image()).placeholder(R.color.colorGreen).into(holder.local_image);
                holder.local_name.setText(game.getOpponent());
                holder.opponent_image.setImageResource(R.drawable.logo);
                holder.opponent_name.setText("Venados F.C.");

            }

            SimpleDateFormat simpleDateFormat =
                    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");

            // Set the date to the holder
            Date theDate = null;
            try {
                theDate = simpleDateFormat.parse(game.getDatetime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            final Date finalTheDate = theDate;
            holder.ic_calendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    programToCalendar(finalTheDate);


                }
            });

            Calendar myCal = new GregorianCalendar();
            myCal.setTime(theDate);

            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
            Date date = null;
            try {
                date = inFormat.parse(game.getDatetime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
            String day = outFormat.format(date);
            String getDay = day.substring(0,3);



            holder.date.setText(String.valueOf(myCal.get(Calendar.DAY_OF_MONTH)));
            holder.day.setText(getDay.toUpperCase());


            holder.home_score.setText(String.valueOf(game.getHome_score()));
            holder.away_score.setText(String.valueOf(game.getAway_score()));


        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return games.size();
    }
    public  void programToCalendar(Date date)
    {
        Calendar myCal = new GregorianCalendar();
        myCal.setTime(date);
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", myCal.getTimeInMillis());
        intent.putExtra("endTime", myCal.getTimeInMillis()+90*60*1000);
        intent.putExtra("title", "Partido de Venados F.C.");
        activity.startActivity(intent);
    }
    @Override
    public int getItemViewType(int position) {
        Log.d("month2", String.valueOf(position));
        if (position == 0) {
        return TYPE_HEAD;
        }
       else
        {
        String oldmonth = null;
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
        Date date = null;
        try {
            date = inFormat.parse(games.get(position).getDatetime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("MMMM");
        String month = outFormat.format(date);

        if (position > 0) {
            SimpleDateFormat oldinFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
            Date olddate = null;
            try {
                olddate = oldinFormat.parse(games.get(position - 1).getDatetime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat oldoutFormat = new SimpleDateFormat("MMMM");
            oldmonth = oldoutFormat.format(olddate);
        }


        if (month.equals(oldmonth)) {

            return TYPE_LIST;


        } else {
            return TYPE_HEAD;

        }
    }




    }



}