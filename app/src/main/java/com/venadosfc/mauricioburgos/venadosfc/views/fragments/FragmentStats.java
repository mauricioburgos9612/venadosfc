package com.venadosfc.mauricioburgos.venadosfc.views.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.adapters.StatsAdapter;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.Constants;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.SharedPreferencesHelper;
import com.venadosfc.mauricioburgos.venadosfc.data.model.stats.ListStats;
import com.venadosfc.mauricioburgos.venadosfc.data.model.stats.Stats;
import com.venadosfc.mauricioburgos.venadosfc.data.remote.service.common.APIService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentStats extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    protected Context context;
    private DrawerLayout drawer;
    @BindView(R.id.staticsToolbar)
    Toolbar toolbar;
    RecyclerView recyclerView;
    StatsAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Stats> stats;
    String[]  position, team,games,score_diff,points;
    int[] image;
    private OnFragmentInteractionListener mListener;
    private APIService mAPIService;
    private SharedPreferencesHelper sharedPreferencesHelper;




    public FragmentStats() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStatics.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStats newInstance(String param1, String param2) {
        FragmentStats fragment = new FragmentStats();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.sharedPreferencesHelper        = new SharedPreferencesHelper();

        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_stats, container, false);
        ButterKnife.bind(this, view);

        //Add the toolbar to the fragment
        final Drawable logo = getResources().getDrawable(R.drawable.ic_statistics);
        logo.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        toolbar.setTitle("Venados F.C");
        toolbar.setSubtitle("Estadísticas");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        toolbar.setLogo(logo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        Gson gson = new Gson();
        String json = sharedPreferencesHelper.get(Constants.STRING_STATS_LIST,"");
        final ListStats listStats = gson.fromJson(json, ListStats.class);
        stats=listStats.getStatistics();




        //give navigation drawer actions
        drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorWhite));
        toggle.syncState();

        //RecyclerView
        recyclerView=(RecyclerView) view.findViewById(R.id.recyclerStats);
        adapter= new StatsAdapter(stats);
        recyclerView.setAdapter(adapter);
        layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);






        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();


        super.onCreateOptionsMenu(menu,inflater);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
