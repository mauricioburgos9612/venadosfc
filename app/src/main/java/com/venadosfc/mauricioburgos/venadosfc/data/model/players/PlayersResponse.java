package com.venadosfc.mauricioburgos.venadosfc.data.model.players;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mauricio Burgos on 09/11/2018.
 */

public class PlayersResponse implements Serializable {
    @SerializedName("sucess")
    private boolean succes;
    @SerializedName("data")
    private DataTeams data_teams;
    @SerializedName("code")
    private int code;

    public PlayersResponse(boolean succes, DataTeams data_teams, int code) {
        this.succes = succes;
        this.data_teams = data_teams;
        this.code = code;
    }

    public boolean isSucces() {
        return succes;
    }

    public void setSucces(boolean succes) {
        this.succes = succes;
    }

    public DataTeams getData_teams() {
        return data_teams;
    }

    public void setData_teams(DataTeams data_teams) {
        this.data_teams = data_teams;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
