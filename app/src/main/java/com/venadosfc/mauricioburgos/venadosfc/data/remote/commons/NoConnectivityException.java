package com.venadosfc.mauricioburgos.venadosfc.data.remote.commons;


import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.Constants;

import java.io.IOException;

public class NoConnectivityException extends IOException {
    @Override
    public String getMessage() {
        return Constants.noConnException;
    }

}
