package com.venadosfc.mauricioburgos.venadosfc.views.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.venadosfc.mauricioburgos.venadosfc.R;
import com.venadosfc.mauricioburgos.venadosfc.adapters.GamesAdapter;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.Constants;
import com.venadosfc.mauricioburgos.venadosfc.data.local.prefs.SharedPreferencesHelper;
import com.venadosfc.mauricioburgos.venadosfc.data.model.games.Games;
import com.venadosfc.mauricioburgos.venadosfc.data.model.games.GamesList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentCopaMx.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentCopaMx#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCopaMx extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Context context;
    @BindView(R.id.recyclerGames)
    RecyclerView recyclerGames;
    private GamesAdapter adapter;
    private SharedPreferencesHelper sharedPreferencesHelper;
    ArrayList<Games> games;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentCopaMx() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentCopaMx.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentCopaMx newInstance(String param1, String param2) {
        FragmentCopaMx fragment = new FragmentCopaMx();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.sharedPreferencesHelper        = new SharedPreferencesHelper();

        View view =inflater.inflate(R.layout.fragment_copa_mx, container, false);
        ButterKnife.bind(this, view);

        //Recover the game object
        Gson gson = new Gson();
        String json = sharedPreferencesHelper.get(Constants.STRING_GAME_LIST,"");
        final GamesList gamesList = gson.fromJson(json, GamesList.class);
        games=gamesList.getGames();

        //Set an adapter to the recyclerView.
        adapter = new GamesAdapter(getGames(games),getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerGames.setLayoutManager(linearLayoutManager);
        recyclerGames.setAdapter(adapter);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public ArrayList<Games> getGames(ArrayList<Games> games) {
        ArrayList<Games> gamesAdapter = new ArrayList<>();

        for (Games games1 : games) {
            if (games1.getLeague().contains("Copa MX")) {
                gamesAdapter.add(games1);
            }
        }

        return gamesAdapter;
    }
}
